﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Apprenda.API.Extension.Bootstrapping;

namespace PlatformModsTestBootstrapper
{
    public class Class1 : BootstrapperBase
    {
        public override BootstrappingResult Bootstrap(BootstrappingRequest bootstrappingRequest)
        {
            var bootstrapper = new Bootstrapper(bootstrappingRequest);
            return bootstrapper.Bootstrap();
        }
    }

    public class Bootstrapper
    {
        private readonly string bootstrapFiles;
        private readonly string bootstrapTarget;

        public Bootstrapper(BootstrappingRequest bootstrappingRequest)
        {
            bootstrapFiles = Path.Combine(bootstrappingRequest.BootstrapperPath, "platform-mods");
            bootstrapTarget = Path.Combine(bootstrappingRequest.ComponentPath, "platform-mods");
        }

        public BootstrappingResult Bootstrap()
        {
            if (!Directory.Exists(bootstrapFiles))
            {
                return BootstrappingResult.Success();
            }

            CopyFile(bootstrapFiles, bootstrapTarget);
            return BootstrappingResult.Success();
        }

        private void CopyFile(string source, string target)
        {
            if (!Directory.Exists(target))
            {
                Directory.CreateDirectory(target);
            }

            foreach (var file in Directory.GetFiles(source))
            {
                File.Copy(file, Path.Combine(target, Path.GetFileName(file)));
            }

            foreach (var directory in Directory.GetDirectories(source))
            {
                CopyFile(directory, Path.Combine(target, Path.GetFileName(directory)));
            }
        }
    }
}
